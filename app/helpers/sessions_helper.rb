module SessionsHelper

  # Logs in the given user
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # Return the current logged-in user (if any)
  # Use find_by instead find (find raise an error whereas find_by return nil)
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  # Return true if the given user is the current user
  def current_user?(user)
    user == current_user
  end
  
  # Is user logged in ? 
  def logged_in?
    !current_user.nil?
  end
  
  
  # Create a persistent session
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  # Forget a persistent session
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
  # Logs out the current user
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
  
  # Redirect to stored location (or to the default)
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  # Store the Url trying to be accessed
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
  
  # Confirms a logged_in user
  def logged_in_user
    unless logged_in?
      store_location  
      flash[:danger] = "Please log in"
      redirect_to login_url
    end
  end

  # Is user admin ? 
  def user_admin?
    current_user.admin?
  end
  
  # Confirms a complete purchase
  def purshase_completed
    if !purchased?
      flash[:warning] = "You have not purchase this tool"
      redirect_to root_url
    end 
  end
  
  def purchased?
    !current_user.transaction_id.nil? || current_user.admin?
  end
  
end
