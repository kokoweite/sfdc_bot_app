class User < ActiveRecord::Base
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  has_secure_password
  # allow_nil: true permit to perform test with empty password (In aprticular when testing edit user feature)
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
  validates :transaction_id, uniqueness: { case_sensitive: false }, allow_nil: true
  
  # Return the hash digest of the given string
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  # Generate a token
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  # Remember a user in the database for use in persistent sessions
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  # Return true if the given token matches the digest
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  # Forget a user
  def forget
    update_attribute(:remember_token, nil)
  end
  
  # Activates an account
  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end
  
  # Send activation mail
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  # Set the reset token
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end
  
  # Send password reset email
  def send_password_reset_email 
    UserMailer.password_reset(self).deliver_now
  end
  
  # Returns true if a password reset has expired
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  # Retrun well formated paypal url
  def paypal_url(return_url,hook_url)
    values = {
        :rm => 1,
        :notify_url => hook_url,
        :business => "#{Rails.application.secrets.vendor}",
        :cmd => "_xclick",
        :upload => 1,
        :return => return_url,
        :invoice => id,
        :amount => "4.99",
        :item_name => "sfdcBot",
        :item_number => "1",
        :quantity => "1"
    }
    "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query
  end
  
  
  private
  
     # Convert email to all lower case
    def downcase_email
      self.email = email.downcase
    end
    
    def create_activation_digest
      self.activation_token = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end
