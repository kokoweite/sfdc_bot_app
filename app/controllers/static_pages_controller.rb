class StaticPagesController < ApplicationController
  before_action :logged_in_user, only: [:download]
  #before_action :purshase_completed, only: [:download]
  
  def home
  end

  def help
  end
  
  def about
  end
  
  def download
  end
  
  def faq
  end
  
  def thanks
    flash.now[:info] = "Thank you for your purshase "
    render 'home'
  end
    
  def download_tool
    response.headers['Content-Length'] = File.size("#{Rails.root}/private/#{params[:id]}").to_s
    send_file("#{Rails.root}/private/#{params[:id]}",
              :filename => "#{params[:id]}",
              :type => "application/zip",
              :x_sendfile => true)
  end
  
end
