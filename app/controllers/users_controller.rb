class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:index, :destroy]
  protect_from_forgery except: [:hook]
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id]);
  end
  
  def new
    @user = User.new
  end
  
  def create 
    @user = User.new(user_params)
    if verify_recaptcha(model: @user) && @user.save
      @user.send_activation_email
      #redirect_to @user.paypal_url(thanks_url, hook_url)
      flash[:success] = "Please check your email to activate your account. Check your spam/junk mail"
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit 
  end
  
  def update 
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def hook
    params.permit! # Permit all Paypal input params
    response = validate_IPN_notification(request.raw_post)
    case response
    when "VERIFIED"
      status = params[:payment_status]
      @user = User.find params[:invoice]

      if status == "Pending" && User.find_by(transaction_id: params[:txn_id]) == nil
        @user.update_attributes notification_params: params, status: status, transaction_id: params[:txn_id], purchased_at: Time.now
      elsif status == "Completed" && @user.transaction_id == params[:txn_id]
        @user.update_attributes notification_params: params, status: status, purchase_completed_at: Time.now
      end
    when "INVALID"
      logger.debug "INVALID"
    else
      logger.debug "ERROR"
    end
    render nothing: true
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  
  private 
  
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
    
    # Before filter
    
    # Confirms the correct user
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms an admin user
    def admin_user
      redirect_to(root_url) unless user_admin?
    end
    
    def validate_IPN_notification(raw)
      uri = URI.parse("#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?cmd=_notify-validate")
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 60
      http.read_timeout = 60
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      http.use_ssl = true
      response = http.post(uri.request_uri, raw,
                           'Content-Length' => "#{raw.size}",
                           'User-Agent' => "My custom user agent"
                         ).body
    end
  
end
