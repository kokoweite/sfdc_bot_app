require 'digest/sha2' 
class ApplicationMailer < ActionMailer::Base
  default "Message-ID"=>"#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@sfdcbot.herokuapp.com"
  default from: "kokoweite@hotmail.com"
  layout 'mailer'
end
