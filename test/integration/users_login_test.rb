require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  
  def setup
    # Load marty user from users.yml file
    @user = users(:marty)
    @user_admin = users(:tagada)
  end
  
  test "login with valid information" do
    log_in_as(@user, { remember_me: '0'})
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", download_path
    assert_select "a[href=?]", users_path, count: 0
    assert_select "a[href=?]", user_path(@user)
  end
  
  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    log_in_as(@user, { remember_me: '0', email: "", password: "" })
    assert_not is_logged_in?
    assert_template 'sessions/new'
    assert_not flash.empty?, "Flash should be not empty"
    get root_path
    assert flash.empty?, "Flash should be empty"
  end
  
  test "login with valid information followed by logout" do
    log_in_as(@user)
    assert is_logged_in?, "User should be logged in"
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
     assert_select "a[href=?]", users_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", download_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    # Simulate a user clicking logout in a seconde window
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end
  
  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    # For some reason we cannot used symbol in integration test
    assert_not_nil cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    # For some reason we cannot used symbol in integration test
    assert_nil cookies['remember_token']
  end
  
  test "login as user admin" do
    log_in_as(@user_admin)
    assert is_logged_in?
    assert_redirected_to @user_admin
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", download_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user_admin)
  end
end
