require 'test_helper'

class MessageMailerTest < ActionMailer::TestCase
 test "message me" do
    msg = Message.new(
      name: 'test',
      email: 'test@example.com',
      subject: 'Hi',
      content: 'foobarbaz'
    )

    email = MessageMailer.message_me(msg).deliver_now

    refute ActionMailer::Base.deliveries.empty?

    assert_equal ['kokoweite@hotmail.com'], email.to
    assert_equal ['test@example.com'], email.from
    assert_equal 'Hi', email.subject
    assert_equal 'foobarbaz', email.body.to_s
  end
end
