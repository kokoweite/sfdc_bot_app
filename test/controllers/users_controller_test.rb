require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  
  def setup
    @user = users(:tagada)
    @marty = users(:marty)
  end
  
  test "should get new" do
    get :new
    assert_response :success
  end
  
  test "should redirect edit when not logged in" do
    get :edit, id: @user
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "shoud redirect update when not logged in" do
    patch :update, id: @user, user: { name: @user.name, email: @user.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "should redirect edit when logged in as wrong user" do
    log_in_as(@marty)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect update when logged in as wrong user" do
    log_in_as(@marty)
    patch :update, id: @user, user: { name: @user.name, email:@user.email }
    assert flash.empty?
    assert_redirected_to root_url
  end
  
  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
  end
  
  test "should not allow the admin attribute to be edited via the web" do 
    log_in_as(@marty)
    assert_not @marty.admin?, "should not be admin"
    patch :update, id:@marty, user: { email: "marty@fly.com",
                                      password: "password",
                                      password_confirmation: "password",
                                      admin: true }
    assert_not @marty.reload.admin?, "should not be admin after illegal update patch request"
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when user logged in but non-admin" do
    log_in_as(@marty)
    assert_no_difference 'User.count' do
      delete :destroy, id: @user
    end
    assert_redirected_to root_url
  end
  
  test "should redirect index when logged as non-admin" do
    log_in_as(@marty)
    get :index
    assert_redirected_to root_url
  end

end
