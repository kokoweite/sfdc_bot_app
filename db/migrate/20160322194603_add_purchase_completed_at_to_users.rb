class AddPurchaseCompletedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :purchase_completed_at, :datetime
  end
end
