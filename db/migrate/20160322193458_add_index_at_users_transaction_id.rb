class AddIndexAtUsersTransactionId < ActiveRecord::Migration
  def change
    add_index :users, :transaction_id, unique: true
  end
end
